const express = require("express")
const bodyParser = require("body-parser")
const mongoose = require("mongoose")

const port = 4000

const application = express()

application.use(bodyParser.urlencoded({ extended: false }));
application.use(bodyParser.json());

// mongoose connection
mongoose.connect('mongodb+srv://earlrafael:2424Diaz!@cluster0.choag.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true 
})

let database = mongoose.connection
database.on('error', console.error.bind(console, 'Connection Error'))
database.once('open', () => console.log('Connected to MongoDB'))

// middleware
application.use(express.json())

// mongoose schema (structure of collection)
const task_schema = new mongoose.Schema({
    name: String,
    status: String
})

// mongoose model (represents collection)
const Task = mongoose.model('task', task_schema)

application.post('/tasks', (request, response) => {
    let new_task = new Task({
        name: request.body.name,
        status: request.body.status
    })

    new_task.save((saved_task, error) => {
        error ? response.send(error) : response.send(saved_task)
    })
})

application.get('/tasks', (request, response) => {
    Task.find({})
        .then(result => response.send(result))
        .catch(err => response.send(err))
})


// ACTIVITY
const user_schema = new mongoose.Schema({
    username: String,
    password: String
})

const User = mongoose.model('user', user_schema)

application.post('/users', (request, response) => {
    let new_user = new User({
        username: request.body.username,
        password: request.body.password
    })

    new_user.save((saved_user, error) => {
        error ? response.send(error) : response.send(saved_user)
    })
})

application.get('/users', (request, response) => {
    User.find({})
        .then(result => response.send(result))
        .catch(err => response.send(err))
})

application.listen(port, () => console.log(`Express server is running on port ${port}`))

